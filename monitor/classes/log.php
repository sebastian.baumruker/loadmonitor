<?php
class Log {
    private $db;
    function __construct($config)
    {
        // db credentials and parameters from the config file. Written apropriately.
        $db = new PDO( 'string $dsn [, string $username [, string $passwd [, array $options ]]]' );
    }

    public function saveLogRow($data) {
        // server was avaliable, so a complete data row can be logged
        if(count($data) > 2) {
            $sql = "INSERT INTO dbName.log (pingStatus, pingStatusMessage, otherColumns) VALUES (:pingStatus, :pingStatusMessage, :otherColumns)";
            $sth = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $sth->execute(array(':pingStatus' => $data["pingStatus"], ':pingStatusMessage' => $data["pingStatusMessage"], ':otherColumns' => $data["otherColumns"]));
        }
        else {
            $sql = "INSERT INTO dbName.log (pingStatus, pingStatusMessage) VALUES (:pingStatus, :pingStatusMessage)";
            $sth = $this->db->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $sth->execute(array(':pingStatus' => $data["pingStatus"], ':pingStatusMessage' => $data["pingStatusMessage"]));
        }
    }
}