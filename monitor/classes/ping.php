<?php

class Ping {
    private $host;
    private $port;
    private $waitTimeoutInSeconds;
    private $status;
    private $statusMessage;

    function __construct($config)
    {
        $this->host = $config->address;
        $this->port = $config->port;
        $this->waitTimeoutInSeconds = $config->waitTimeoutInSeconds;
        $this->status = $config->defaultStatus;
        $this->statusMessage = $config->defaultStatusMessage;
    }

    public function doPing() {
        if($fp = fsockopen($this->host, $this->port, $errCode, $errStr, $this->waitTimeoutInSeconds)){   
            $this->status = 1;
            $this->statusMessage = "Server is running";
        } else {
            $this->status = 0;
            $this->statusMessage = $errCode.' - '.$errStr;
        } 
        fclose($fp);
    }

    public function getStatus() {
        return $this->status;
    }

    public function getStatusMesage() {
        return $this->statusMessage;
    }
}