// opens, controls and feeds the socket channel that the front end will subscribe to
// when the listen.js presents new data this modual will broadcast it to the appropriate channel/s

// the data will be validates in here as well and sorted in one of three cateories

// 1. Server down -> if the ping returned that the machine is not running it will be broadcast into a 
// channel linked to systems for immediat action such as mailings and push messages

// 2. Load above thresholds -> if the data validation shows the load is above the set load thresholds 
// the message will be broadcast into both the regular data channel for displaying in the front end 
// as well as an emergancy channel for urgend but not immediat action

// 3. Load is bellow the threshold -> message will be broadcast into the regular data channel for 
// displaying in the front end