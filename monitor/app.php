<?php

// app will run once per second triggert via a cronjob.
// if the process itself needs to be validated if it is 
// running or not I would use a shell scrippt between 
// the cron and the app

include_once("classes/ping.php");
include_once("classes/log.php");

$configJSON = file_get_contents("config/config.json");
$config = json_decode($configJSON, true);
$statusData = array();

foreach($config["hosts"] as $host => $val) {
    $ping = new Ping($val);
    $ping->doPing();
    $statusData[$host] = $val;
    $statusData[$host]["pingStatus"] = $ping->getStatus();
    $statusData[$host]["pingStatusMessage"] = $ping->getStatusMesage();

    if($ping->getStatus() === 1) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $val->address."/[PATH_TO_RUN_SCRIPT]");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $statusData[$host]["dbStatus"] = $output;
        curl_close($ch);
    }
}

$log = new Log($config["logDB"]);

foreach($statusData as $host) {
    $log->saveLogRow($host);
}