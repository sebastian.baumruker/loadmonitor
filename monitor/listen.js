// listens to changes of the logDB (e.g. using a lib like https://www.npmjs.com/package/mysql-events)
// when an insert event is fired, select the new data and pipe them to the broadcast.js module