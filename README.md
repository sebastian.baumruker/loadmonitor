# LoadMonitor

The purpose of this tool is to monitor several databases. It will display this data in aproximately 
real time in a web based front end. Additionally it will send out warnings and alerts if certain 
configurable thresholds are crossed to allow for faster intervening responses before the system reaches 
a critical or performance hindering state.

This is not a working application but a pseudo code to showcase my thought processes, choices in tech stack, 
concepts for program flow and component communication.