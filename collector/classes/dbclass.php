<?php

class DBClass {
    /* 
        your database class goes here
        this needs to be able to perform the following tasks:

            SELECT * FROM INFORMATION_SCHEMA.PROCESSLIST ORDER BY STATE DESC;
            SELECT count(*) FROM INFORMATION_SCHEMA.PROCESSLIST;

            SHOW GLOBAL STATUS;

            SHOW GLOBAL VARIABLES LIKE '%table_size';
            SHOW TABLE STATUS;
            SHOW OPEN TABLES;

            SHOW GLOBAL VARIABLES LIKE '%buffer%';

            SHOW GLOBAL VARIABLES LIKE '%query_cache%';

            SHOW GLOBAL VARIABLES LIKE 'ft_%';
    */

    public function getSomeMetric() {
        /* getter for a specific metric */
    }
}