<?php
class Collector {
    private $dbStatus;
    private $db;
    
    /* setup the variables and db connection */
    function __construct($db) {
        $this->dbStatus = [];
        $this->db = $db;
        $this->readStatus();
   }

   /* read all the status metrics from the db */
    private function readStatus() {
        $this->dbStatus["AStatusMetric"] = $this->db->getSomeMetric();
        /* ... */
    }

    /* return the status array */
    public function getStatus() {
        return $this->dbStatus;
    }
}
