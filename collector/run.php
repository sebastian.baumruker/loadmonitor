<?php
require_once("classes/dbclass.php");
require_once("classes/collector.php");

/* initialize new collector instance */
$db = new DBClass();
$collector = new Collector($db);

/* return the status to the caller */

return $collector->getStatus();